from django.shortcuts import render

# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

# Local imports
from .models import GroceryItem

# to use the template created:
from django.template import loader

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	template = loader.get_template("index.html")
	context = {
		'groceryitem_list': groceryitem_list
	}
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
	return HttpResponse(template.render(context, request))

def groceryitem(request, groceryitem_id):
	response = f"You are viewing the details of {groceryitem_id}"
	return HttpResponse(response)